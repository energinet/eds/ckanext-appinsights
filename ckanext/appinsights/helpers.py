import os

try:
    # CKAN 2.7 and later
    from ckan.common import config
except ImportError:
    # CKAN 2.6 and earlier
    from pylons import config

def eds_version():
    return os.environ.get('RELEASE_VERSION')

def get_instrumentation_key():
   return config.get('ckanext.appinsights.instrumentation_key') 
