import ckan.plugins as plugins
import ckan.plugins.toolkit as toolkit
from ckanext.appinsights.requestlogger import RequestLogger
from ckanext.appinsights import helpers


class AppinsightsPlugin(plugins.SingletonPlugin):
    plugins.implements(plugins.IConfigurer)
    plugins.implements(plugins.IMiddleware, inherit=True)
    plugins.implements(plugins.ITemplateHelpers)

    # IMiddleware
    def make_error_log_middleware(self, app, config):
        inst_key = config.get('ckanext.appinsights.instrumentation_key')
        return RequestLogger(inst_key, app, config)

    # IConfigurer

    def update_config(self, config_):
        toolkit.add_template_directory(config_, 'templates')
        toolkit.add_public_directory(config_, 'public')
        toolkit.add_resource('fanstatic', 'ckanext-appinsights')

    # ITemplateHelpers

    def get_helpers(self):
        return {
            'eds_version': helpers.eds_version,
            'get_instrumentation_key': helpers.get_instrumentation_key
        }
