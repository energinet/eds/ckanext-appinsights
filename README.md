## ckanext-appinsights

This extension enables the administrator to ship metrics to Azure's Application Insights.

Each request is send to AppInsights and ervery webpage has a JS library injected that
sends browser data to AppInsights. This enables AppInsights to show both "Server" and
"Browser" metrics.

No attempt is made to identify users or individual sessions and we attempt to collect as
little PII as possible.

You should note that the provided AppInsights Instrumentation Key is send to the client,
and is trivial for competent users to retrieve. If that is not okay with you, please fork
this extension.

If the environment variable 'RELEASE_VERSION' is set, the value is used to identify the
application version in AppInsights.

### Known Issues

Due to the way CKAN handles exceptions, we haven't been able to get this extension to
capture exceptions, log them to AppInsights and hand the exeption back to CKAN in a
stable manner. Thus, exeptions isn't logged.

### Requirements

This should work with CKAN 2.6.4+

### Installation

To install ckanext-appinsights:

1. Activate your CKAN virtual environment, for example::

     . /usr/lib/ckan/default/bin/activate

2. Install the ckanext-appinsights Python package into your virtual environment::

     pip install ckanext-appinsights

3. Add ``appinsights`` to the ``ckan.plugins`` setting in your CKAN
   config file (by default the config file is located at
   ``/etc/ckan/default/production.ini``).

4. Restart CKAN. For example if you've deployed CKAN with Apache on Ubuntu::

     sudo service apache2 reload


### Config Settings

Provide your Instrumentation Key

ckanext.appinsights.instrumentation_key = some-really-long-key

### Running the Tests

To run the tests, do::

    nosetests --nologcapture --with-pylons=test.ini

To run the tests and produce a coverage report, first make sure you have
coverage installed in your virtualenv (``pip install coverage``) then run::

    nosetests --nologcapture --with-pylons=test.ini --with-coverage --cover-package=ckanext.appinsights --cover-inclusive --cover-erase --cover-tests


